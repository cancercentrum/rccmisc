[![CRAN_Status_Badge](http://www.r-pkg.org/badges/version/rccmisc)](https://cran.r-project.org/package=rccmisc)
[![Build status](https://ci.appveyor.com/api/projects/status/94b5vio9ffmn0tu6?svg=true)](https://ci.appveyor.com/project/erik_bulow/rccmisc)
![Monthly downloads](http://cranlogs.r-pkg.org/badges/rccmisc) 
![Total downloads](http://cranlogs.r-pkg.org/badges/grand-total/rccmisc)
[![Project Status: Active - The project has reached a stable, usable state and is being actively developed.](http://www.repostatus.org/badges/latest/active.svg)](http://www.repostatus.org/#active)

rccmisc
========

Miscellaneous R functions for the Regional Cancer Centers.
This package is intended as one of the dependent packages for `rcc2`.

# Install

If not installed as dependent for `rcc2`, it can also be installed using `devtools`:
```{r}
devtools::install_bitbucket("cancercentrum/rccmisc")
```

Introduction
========

The package contains vignettes! Please use `browseVignettes("rccmisc")` after installing or watch online:
https://htmlpreview.github.io/?https://bitbucket.org/cancercentrum/rccmisc/raw/0d36cbf51632e5bc5e6ee61b099a395bb542629f/vignettes/rccmisc.html